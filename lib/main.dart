import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
      listTileTheme: ListTileThemeData (
        iconColor: Colors.indigo.shade500,
      )
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black45,
        iconTheme: IconThemeData(
            color: Colors.white70
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.lightBlueAccent.shade400,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {

  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK ? MyAppTheme.appThemeDark() : MyAppTheme.appThemeLight(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(currentTheme == APP_THEME.DARK ? Icons.light_mode_outlined : Icons.dark_mode_outlined),
          backgroundColor: currentTheme == APP_THEME.DARK ? Colors.white60 : Colors.black45,
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK ? currentTheme = APP_THEME.LIGHT : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call")
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.chat,
        ),
        onPressed: () {},
      ),
      Text("Text")
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
        ),
        onPressed: () {},
      ),
      Text("Video")
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        ),
        onPressed: () {},
      ),
      Text("Email")
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        ),
        onPressed: () {},
      ),
      Text("Directions")
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
        ),
        onPressed: () {},
      ),
      Text("Pay")
    ],
  );
}

Widget mobilePhoneListTile(String phoneNum) {
  return ListTile(
    leading: Icon(
      Icons.call,
      color: Colors.grey,
    ),
    title: Text(phoneNum),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile(String phoneNum) {
  return ListTile(
    leading: Text(""),
    title: Text(phoneNum),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: () {},
    ),
  );
}

Widget emailListTile(String email) {
  return ListTile(
    leading: Icon(
        Icons.email,
        color: Colors.grey,
    ),
    title: Text(email),
    subtitle: Text("work"),
  );
}

Widget addressListTile(String address) {
  return ListTile(
    leading: Icon(
        Icons.location_on,
        color: Colors.grey,
    ),
    title: Text(address),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      onPressed: () {},
    ),
  );
}

buildAppBarWidget() {
  return AppBar(
    leading: IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        print("Back");
      },
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        onPressed: () {
          print("Contact is marked");
        },
      )
    ],
  );
}

buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            child: Image.network(
              "https://i.ytimg.com/vi/3W13Sj-Jzsw/maxresdefault.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    "Nishikigi Chisato",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
            thickness: 1,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: profileActionItem(),
          ),
          Divider(
            color: Colors.grey,
            thickness: 1,
          ),
          mobilePhoneListTile("050-151-0051"),
          otherPhoneListTile("052-079-8186"),
          Divider(
            color: Colors.grey,
            thickness: 1,
          ),
          emailListTile("nishikigi.ricoricoCafe@gmail.com"),
          addressListTile("346-1133, Lycoris Recoil Cafe")
        ],
      ),
    ],
  );
}

Widget profileActionItem() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
